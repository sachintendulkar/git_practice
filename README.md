# BASIC GIT WORKFLOW

Git is a software that allows you to keep track of changes made to a project over time. Git works by recording the changes you make to a project, storing those changes, then allowing you to reference them as needed.

Learn git work flow by practice

1. Create a folder
2. Add some files 
3. Now that we have started working on the project, turn this directory into a Git project.
###`git init`

    The word `init` means initialize. The command sets tup all the tools Git needs to begin tracking changes made to the project.

    Activate git terminal and run the init command

4. A Git project can be though of as having three parts:

![Image of Yaktocat](images/workflow.png)

* A Working Directory: where you'll be doing all the work: creating, editing, deleting and organizing files
* A Staging Area: where you'll list changes you make to the working directory
* A Repository: where Git permanently stores those changes as different versions of the project


The Git workflow consists of editing files in the working directory, adding files to the staging area, and saving changes to a Git repository. In Git, we save changes with a commit.

###`git status`

###`git add`

In order for Git to start tracking, the file needs to be added to the staging area.

We can add a file to the staging area with:
`git add filename`

###`git diff`

###`git commit`



